# tweetr

## Project setup
```
yarn install
```

There are seeds for basic data in `src/utils/index.js`, it runs automatically on inital load

`localStorage.clear()` can be run on the console to "reset" the application back to it's initial state, don't forget to refresh the page after clearing localStorage

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Planing
does writing a "plain" post and mentioning another user with @ count as a reply? or do I have to specifically click a "reply" button?

How many levels does it go? Does a reply reply to one tweet only, or can it be a reply to a reply from different users? What's the business rule for that?

does it only count as a reply if the @mention is at the beggining of the text? or can the users edit the text and keep the @mention wherever they want?

If replies "should not be shown in the homepage feed", how are other users supposed to see them? is it only by accessing a user's profile and going to that specific tab?

There's already a way to see all original posts from an user, shouldn't the reply tab be replies only?

### Implementation
I would add a `replyTo` property on the tweets to define whether they are or not a reply. Having that it would be easy to know when a tweet is a reply and what it's replying to, so the UI could be built in any way envisioned by stakeholders and UX designers

## Critique
>improvements
- I would improve the design and do a better job with reusability with css, something I didn't do due to the lack of proper planning before executing. 
- The store modules could also be re-arranged in a better way that avoids cross-module dispatching and be clearer on its intent and readability
- I would've love to have added transitions, animations and micro-interactions, I think they help bring a sense of joy to the user (and also helps hiding loadings)
- I have added and dark mode by changing css custom properties, but I think it looks awful, someone who really knows colors could easily fix it just by changing the variables at app.vue
>scaling
- While keeping a id-indexed map of the tweets helps when searching individual ones to display on quotes and reposts, it's an excessive usage of memory, and perhaps the first thing to fail when scaling
- I'd bring all of that to the backend, and whenever there's a list of tweets they would be SSR-ed to avoid blowing up memory constraints on mobile devices
- A lot of tests would have to be added as well, it's not anywhere safe for running on production, or with more than 5 users 😬