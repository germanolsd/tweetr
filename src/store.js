import { createStore } from 'vuex'
import { tweets, user, users } from './store-modules'

// Create a new store instance.
const store = createStore({
  modules: {
    tweets,
    user,
    users
  }
})

export default store