import { loremIpsum } from "lorem-ipsum";

const seedUser = () => {
  localStorage.setItem('user', JSON.stringify({
    userId: '5',
    name: 'Germano Lisboa',
    email: 'germanolisboa@outlook.com',
    following: ['3'],
    followers: ['3'],
    joined: '2021-11-04',
    postCount: {
      date: null,
      value: 0
    }
  }))
}

const seedUsers = () => {
  localStorage.setItem('users', JSON.stringify([
    {
      userId: '1',
      name: 'user1',
      email: 'user1@posterr',
      joined: '2022-01-01',
      following: ['2'],
      followers: ['2','3']
    },
    {
      userId: '2',
      name: 'user2',
      email: 'user2@posterr',
      joined: '2022-01-04',
      following: ['1','3'],
      followers: ['1','3','4']
    },
    {
      userId: '3',
      name: 'user3',
      email: 'user3@posterr',
      joined: '2022-02-02',
      following: ['2','5'],
      followers: ['1','2','5']
    },
    {
      userId: '4',
      name: 'user4',
      email: 'user4@posterr',
      joined: '2022-01-02',
      following: ['2'],
      followers: ['1','2']
    }
  ]))
}

const seedTweets = () => {
  const tweets = [
    {
      text: loremIpsum(),
      user: {
        userId: '1',
        userName: 'user1' 
      },
      created_at: '2022-01-02T14:48:00.000Z',
      repostOf: null,
      quoteOf: null,
      id: '1'
    },
    {
      text: loremIpsum(),
      user: {
        userId: '2',
        userName: 'user2' 
      },
      created_at: '2022-01-02T14:48:00.000Z',
      repostOf: null,
      quoteOf: '1',
      id: '2'
    },
    {
      text: loremIpsum(),
      user: {
        userId: '2',
        userName: 'user2' 
      },
      created_at: '2022-01-02T14:55:23.000Z',
      quoteOf: null,
      repostOf: null,
      id: '3'
    },
    {
      text: '',
      user: {
        userId: '4',
        userName: 'user4' 
      },
      created_at: '2022-02-02T14:55:23.000Z',
      quoteOf: null,
      repostOf: '2',
      id: '4'
    },
    {
      text: loremIpsum(),
      user: {
        userId: '3',
        userName: 'user3' 
      },
      created_at: '2022-01-14T14:55:23.000Z',
      quoteOf: null,
      repostOf: null,
      id: '5'
    }
  ]

  localStorage.setItem('tweets', JSON.stringify(tweets))
}

export {
  seedTweets,
  seedUser,
  seedUsers
}