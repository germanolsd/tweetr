import { useStore } from 'vuex'
import { onMounted, computed } from 'vue'

const useInitalFetch = () => {
  const store = useStore()
  const fetchTweets = () => store.dispatch('tweets/fetchTweets')
  const fetchUsers = () => store.dispatch('users/fetchUsers')
  const fetchMe = () => store.dispatch('user/fetchUser')
  onMounted(() => {
    fetchTweets()
    fetchUsers()
    fetchMe()
  })
}

const getLoggedUser = () => {
  const store = useStore()
  const loggedUser = computed(() => store.getters['user/getMe'])

  return loggedUser
}

export {
  useInitalFetch,
  getLoggedUser
}