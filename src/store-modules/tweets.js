import {orderBy} from 'lodash'
import { getTweets, saveTweet } from '@/api/api'

const Tweets = {
  namespaced: true,
  state: {
    tweetList: [],
    isQuoteOf: null
  },
  getters: {
    getTweetList(state) {
      return orderBy(state.tweetList, 'created_at', 'desc')
    },
    getTweetsByUserId: state => id => {
      if (Array.isArray(id)) {
        return state.tweetList.filter(tweet => {
          return id.indexOf(tweet.user?.userId) >= 0
        })
      }
      if (id) {
        return state.tweetList.filter(tweet => (tweet.user?.userId) === id)
      }
      return []
    },
    getIsQuoteOf (state) {
      return state.isQuoteOf
    },
    getLinkedTweetList (state) {
      const linked = {}
      state.tweetList.forEach(tweet => {
        linked[tweet.id] = tweet
      })
      return linked
    },
    getTweetById: (state, getters) => id => {
      return getters.getLinkedTweetList[id]
    } 
  },
  mutations: {
    setTweetList (state, list) {
      state.tweetList = list
    },
    addTweet (state, tweet) {
      state.tweetList = [...state.tweetList, tweet]
    },
    setQuoteOf (state, value) {
      state.isQuoteOf = value
    }
  },
  actions: {
    async fetchTweets ({commit}) {
      const tweets = await getTweets()
      commit('setTweetList', tweets)
    },
    async newTweet ({dispatch}, tweet) {
      await saveTweet(tweet)
      dispatch('fetchTweets')
      return true
    },
    setQuoteTweet ({commit}, value) {
      commit('setQuoteOf', value)
    }
  }
}

export default Tweets