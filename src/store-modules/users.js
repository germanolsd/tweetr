import {getUsers, saveUsers} from '../api/api'

const Users = {
  namespaced: true,
  state: {
    users: []
  },
  mutations: {
    setUsers(state, userList) {
      state.users = userList
    }
  },
  actions: {
    async fetchUsers({commit}) {
      const res = await getUsers()
      commit('setUsers', res)
    },
    async addUserFollower({commit, getters}, {userId, followerId}) {
      const users = getters.getUserList
      const newUserList = users.map(loopUser => {
        if (loopUser.userId === userId) {
          return {
            ...loopUser,
            followers: [...loopUser.followers, followerId]
          }
        }
        return loopUser
      })
      await saveUsers(newUserList)
      commit('setUsers', newUserList)
    },
    async removeUserFollower({commit, getters}, {userId, followerId}) {
      const users = getters.getUserList
      const newUserList = users.map(loopUser => {
        if (loopUser.userId === userId) {
          return {
            ...loopUser,
            followers: loopUser.followers.filter(id => id !== followerId)
          }
        }
        return loopUser
      })
      await saveUsers(newUserList)
      commit('setUsers', newUserList)
    }
  },
  getters: {
    getUserList (state) {
      return state.users
    },
    getUserById: state => id => {
      if (id) {
        return state.users.find(user => String(user.userId) === String(id))
      }
      return null
    }
  }
}

export default Users