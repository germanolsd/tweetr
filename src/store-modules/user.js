import {getUser, editUser} from '../api/api'
import { isSameDay } from 'date-fns'

const User = {
  namespaced: true,
  state: {
    me: {
      userId: null,
      name: '',
      email: '',
      following: [],
      followers: [],
      joined: '',
      postCount: {
        date: null,
        value: 0
      }
    },
    preference: {
      darkMode: false
    }
  },
  mutations: {
    defineUser(state, user) {
      state.me = user
    },
    setPostsCounter(state, value) {
      editUser({
        postCount: {
          ...state.me.postCount,
          ...value
        }
      })
      state.me.postCount = {
        ...state.me.postCount,
        ...value
      }
    },
    setDarkMode (state) {
      state.preference.darkMode = !state.preference.darkMode
    }
  },
  actions: {
    toggleDarkMode ({commit}) {
      commit('setDarkMode')
    },
    async fetchUser({commit}) {
      const res = await getUser()
      commit('defineUser', res)
    },
    async addFollowing({state, commit, dispatch}, followingId) {
      const newUserData = {
        ...state.me,
        following: [...state.me.following, followingId]
      }
      const itWorked = await editUser(newUserData)
      if (itWorked) {
        commit('defineUser', newUserData)
        dispatch('users/addUserFollower', {
          userId: followingId,
          followerId: state.me.userId
        }, {root: true})
      }
    },
    async removeFollowing({state, commit, dispatch}, followingId) {
      const newUserData = {
        ...state.me,
        following: state.me.following.filter(id => id !== followingId)
      }
      const itWorked = await editUser(newUserData)
      if (itWorked) {
        commit('defineUser', newUserData)
        dispatch('users/removeUserFollower', {
          userId: followingId,
          followerId: state.me.userId
        }, {root: true})
      }
    },
    addPostCounter({commit, state}) {
      if (!state.me.postCount?.date) {
        commit('setPostsCounter', {
          date: new Date(),
          value: 1
        })
        return true
      }
      const sameDay = isSameDay(state.me.postCount?.date, new Date())
      if (sameDay) {
        if (state.me.postCount?.value < 5) {
          commit('setPostsCounter', {
            value: state.me.postCount?.value + 1
          })
          return true
        } else {
          return false
        }
      } else {
        commit('setPostsCounter', {
          date: new Date(),
          value: 1
        })
      }
      return false
    }
  },
  getters: {
    getMe (state) {
      return state.me
    },
    getMyTweets (state, getters, rootState, rootGetters) {
      if (state?.me?.id) {
        return rootGetters['tweets/getTweetList'].filter(tweet => tweet.userId = state.me.id)
      }

      return []
    },
    canUserPostToday (state) {
      if (!state.me.postCount?.date) {
        return true
      }
      const sameDay = isSameDay(state.me.postCount.date, new Date())
      return (sameDay && state.me.postCount?.value < 5)
    },
    isDarkMode (state) {
      return state.preference.darkMode
    }
  }
}

export default User