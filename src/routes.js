import { createWebHashHistory, createRouter} from 'vue-router'
import StartPage from './views/StartPage.vue'

export const NAMED_ROUTES = {
  ALL: 'all',
  FOLLOWING: 'following',
  HOME: 'home'
}

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      component: StartPage,
      redirect: '/following',
      name: NAMED_ROUTES.HOME,
      props: { followingOnly: true }
    },
    {
      path: '/following',
      component: StartPage,
      name: NAMED_ROUTES.FOLLOWING,
      props: { followingOnly: true }
    },
    {
      path: '/all',
      component: StartPage,
      name: NAMED_ROUTES.ALL,
      props: { followingOnly: false }
    }
  ]
})

export default router
