import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './routes'
import vClickOutside from "click-outside-vue3"

const app = createApp(App)
app.use(router)
app.use(store)
app.use(vClickOutside)

app.mount('#app')
