import { seedTweets, seedUser, seedUsers } from '../utils'
import {uid} from 'uid'
import { isEmpty } from 'lodash'
import { formatISO } from 'date-fns'

const getUsers = () => {
  if (isEmpty(localStorage.users)) {
    seedUsers()
  }
  return new Promise(resolve => {
    resolve(JSON.parse(localStorage.getItem('users')))
  })
}

const saveUsers = users => {
  localStorage.setItem('users', JSON.stringify(users))
  return new Promise(resolve => {
    resolve(true)
  })
}

const getUser = () => {
  if (isEmpty(localStorage.user)) {
    seedUser()
  }
  return new Promise(resolve => {
    resolve(JSON.parse(localStorage.getItem('user')))
  })
}

const editUser = (newData) => {
  const currentUserData = JSON.parse(localStorage.getItem('user'))
  localStorage.setItem('user', JSON.stringify({
    ...currentUserData,
    ...newData
  }))
  return new Promise(resolve => {
    resolve(true)
  })
}

const saveTweet = newTweet => {
  const tweets = JSON.parse(localStorage.getItem('tweets'))
  localStorage.setItem('tweets', JSON.stringify([...tweets, {
    ...newTweet,
    id: uid(),
    created_at: formatISO(new Date())
  }]))

  return new Promise(resolve => {
    resolve(true)
  })
}

const getTweets = () => {
  if (isEmpty(localStorage.tweets)) {
    seedTweets()
  }
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(JSON.parse(localStorage.getItem('tweets')))
    }, 500)
  })
}

export {
  getUser,
  getTweets,
  saveTweet,
  getUsers,
  editUser,
  saveUsers
}